#!/usr/bin/python

#import BaseHTTPServer
#import SimpleHTTPServer
import cgi
import requests
import ssl
import sys
import os
import gevent
from gevent import pywsgi


from pysnmp.hlapi import *
from pyzabbix import ZabbixAPI

os.environ['GEVENT_RESOLVER'] = 'thread'
os.environ['GEVENTARES_SERVERS'] = '8.8.8.8'

p_user = 'hydra_get_img'
p_password = ''
loginurl = ''
logindata = {'autologin': '1', 'name': p_user, 'password': p_password, 'enter': 'Sign in'}
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0',
           'Content-type': 'application/x-www-form-urlencoded'}
global zapi, graph_img, session, graphid, sid


def auth(loginurl, p_user, p_password):
    try:
        zapi = ZabbixAPI(loginurl)
        zapi.login(p_user, p_password)
        zapi.session.verify = False
        return zapi
    except:
        sys.exit("Error: No connection to the Zabbix API ")


def get_id(host_ip, host_port, zapi):
    try:
        graph_id = zapi.graph.get(output="shorten", filter={"host": host_ip}, sortfield="name")
        graph_id[host_port] = str(graph_id[host_port])
        return graph_id[host_port][15:-2]
    except:
        print("Error: Can't get graph_id")


def get_graph_img(session, loginurl, graphid, period, sid, width, height):
    try:
        if session.cookies['zbx_sessionid']:
            graph_url = loginurl + '/chart2.php?graphid=%s&period=%s&sid=%s&width=%s&height=%s' % (
                graphid, period, sid, width, height)
            graph_req = session.get(graph_url)
            return graph_req.content
    except:
        print("Error: Can't get Zabbix img")


def get_snmp(host_ip, host_port):
    host_port = host_port + 5;
    errorIndication, errorStatus, errorIndex, varBinds = next(
        getCmd(SnmpEngine(),
               CommunityData('public123', mpModel=0),
               UdpTransportTarget((host_ip, 161)),
               ContextData(),
               ObjectType(ObjectIdentity('IF-MIB', 'ifAdminStatus', host_port)),
               ObjectType(ObjectIdentity('IF-MIB', 'ifOperStatus', host_port)),
               ObjectType(ObjectIdentity('IF-MIB', 'ifSpeed', host_port)),
               ObjectType(ObjectIdentity('IF-MIB', 'ifInErrors', host_port)),
               ObjectType(ObjectIdentity('IF-MIB', 'ifOutErrors', host_port)),
               )
    )
    val = [];
    if errorIndication:
        print(errorIndication)
    elif errorStatus:
        print('%s at %s' % (errorStatus.prettyPrint(),
                            errorIndex and varBinds[int(errorIndex) - 1][0] or '?'))
    else:
        for key, value in varBinds:
            val.append(str(value))
    return ','.join(val)


def get_zab():
    for key, value in os.environ.iteritems():
        if key == 'QUERY_STRING':
            value = value.split("&")
            val2 = []
            for val in value:
                val2.append(val[val.find('=') + 1:])
            graphid = get_id(str(val2[0]), int(val2[1]), zapi)
            graph_img = get_graph_img(session, loginurl, graphid, int(val2[2]), sid, int(val2[3]), int(val2[4]))
    return graph_img


def cgi_app(environ, start_response):
    status = '200 OK'
    headers = [('Content-type', 'image/png'),
               ('Access-Control-Allow-Headers', 'Origin, X-Requested-With, X-Prototype-Version, Content-Type, Accept'),
               ('Access-Control-Allow-Origin', '*'),
               ('Access-Control-Expose-Headers', 'x-json'),
               ]
    #

    start_response(status, headers)
    for key, value in environ.iteritems():
        if key == 'QUERY_STRING':
            value = value.split("&")
            val2 = []
            for val in value:
                val2.append(val[val.find('=') + 1:])
            if val2[0] == "0":
                graphid = get_id(str(val2[1]), int(val2[2]), zapi)
                graph_img = get_graph_img(session, loginurl, graphid, int(val2[3]), sid, int(val2[4]), int(val2[5]))
                return graph_img
            elif val2[0] == "1":
                return get_snmp(str(val2[1]), int(val2[2]))
            else:
                return "none"

zapi = auth(loginurl, p_user, p_password)
session = requests.session()
login = session.post(loginurl, params=logindata, headers=headers)
sid = session.cookies['zbx_sessionid'][17:]

httpd = pywsgi.WSGIServer(('ip', port), cgi_app, certfile='/var/adv_scripts/certs/server_8456.pem')
httpd.serve_forever()
